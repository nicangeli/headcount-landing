import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import classes from './HomeView.scss'
import classnames from 'classnames'
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import { actions as authActions } from 'redux/modules/auth'
import AppBar from 'material-ui/lib/app-bar';
import background from './bg.png'
import logo from './logo.png'
import screenshot1 from './screenshot1.png'
import screenshot2 from './screenshot2.png'
import { StickyContainer, Sticky } from 'react-sticky'

const mapStateToProps = (state) => ({
})
export class HomeView extends Component {
  register () {
    const email = this.refs.email.value
    this.props.setEmail(email)
  }
  render () {
    let styles = {
      background: `url(${background}) no-repeat center center fixed`,
      backgroundSize: 'cover',
      height: '90vh'
    }
    let screenshotStyle = {
      fontFamily: 'OpenSans-Bold',
      fontSize: '20',
      color: '#333333'
    }
    return (
      <div>
        <div style={styles}>
          <StickyContainer>
            <Sticky>
              <div className={classnames('top-bar', classes.topBar)}>
                <div className='top-bar-left'>
                  <img style={
                      {
                        width: '150px',
                        margin: '36px 0 0 116px'
                      }
                    } src={`${logo}`} />
                </div>
                <div className='top-bar-right'>
                  <ul className={classnames('menu', classes.menuItems)}>
                    <li>About</li>
                    <li>Pricing</li>
                    <li>Log In</li>
                  </ul>
                </div>
              </div>
            </Sticky>
          </StickyContainer>
          <div style={{'paddingTop': '30px'}} className='row'>
            <div className='small-12 columns'>
              <div className={classes.container}>
                <h1 style={
                    {
                      fontFamily: 'OpenSans-Bold',
                      fontSize: '50px',
                      color: '#FFFFFF',
                      lineHeight: '66px',
                      textShadow: '0px 1px 1px rgba(0,0,0,0.50)',
                      maxWidth: '700px',
                      margin: '0 auto',
                      padding: '100px 0 45px 0'
                    }
                  }>The simplest careers site for your startup</h1>
                <input
                  style={{
                    background: '#FFFFFF',
                    borderRadius: '5px',
                    fontFamily: 'OpenSans-Semibold',
                    fontSize: '18px',
                    color: '#5F6C72',
                    letterSpacing: '1px',
                    lineHeight: '45px',
                    border: 'none',
                    padding: '0 20px'
                  }}
                  placeholder='email@company.com'
                  ref='email' />
                <RaisedButton
                  style={{
                    marginLeft: '10px',
                    lineHeight: '45px',
                    fontSize: '18px',
                    height: '45px',
                    width: '200px'
                  }}
                  backgroundColor='#13D0AB'
                  onClick={this.register.bind(this)}
                  label="Start 14 Day Trial"
                  secondary={true} />
              </div>
            </div>
          </div>
        </div>
        <div className='row'>
          <h2 className={classnames('small-12 columns', classes.subtitle)}>Quick & Easy</h2>
          <div className='small-12 medium-4 columns'>
            <h3 className={classnames(classes.explainer, classes.briefcase)}>Create your company profile</h3>
          </div>
          <div className='small-12 medium-4 columns'>
            <h3 className={classnames(classes.explainer, classes.listings)}>Upload Job Listings</h3>
          </div>
          <div className='small-12 medium-4 columns'>
            <h3 className={classnames(classes.explainer, classes.person)}>Track Applicants</h3>
          </div>
        </div>
        <div className='row'>
          <h2 className={classnames('small-12 columns', classes.subtitle)}>About</h2>
          <div className='small-12 medium-6 columns'>
            <img
              style={
                {
                  width: '320',
                  float: 'right',
                  marginRight: '60'
                }
              }
              src={`${screenshot1}`}
            />
          </div>
          <div className='small-12 medium-6 columns'>
            <div>
              <h3 style={screenshotStyle}>Upload a job</h3>
              <p style={{
                  maxWidth: '270',
                }}>
                Use the Headcount platform to keep your jobs up to date, add custom job questions and stuff to do with a careers site here.
              </p>
            </div>
          </div>
          <div className='small-12 medium-6 columns'>
            <div style={
                {
                  float: 'right',
                  marginRight: '110',
                  marginTop: '40'
                }
              }>
              <h3 style={screenshotStyle}>Track applicants</h3>
              <p style={{
                  maxWidth: '270'
                }}>
                Track job applicants through the Headcount platform. Be notified when someone applies or looks at your job posting. View suggested applicants for your job and reach out directly to them.
              </p>
            </div>
          </div>
          <div className='small-12 medium-6 columns'>
            <img
              style={
                {
                  width: '290',
                  marginTop: '40'
                }
              }
              src={`${screenshot2}`}
            />
          </div>
        </div>
        <div className='row'>
          <h2 className={classnames('small-12 columns', classes.subtitle)}>Pricing</h2>
          <div className='small-7 large-centered columns'>
            <div className='row'>
              <div className='small-12 medium-4 columns'>
                <div className={classnames(classes.pricing)}>
                  <span className={classnames(classes.plan)}>Basic</span>
                  <span className={classnames(classes.price)}>£10 <span className={classnames(classes.month)}>/ MONTH</span></span>
                </div>
              </div>
              <div className='small-12 medium-4 columns'>
                <div className={classnames(classes.pricing)}>
                  <span className={classnames(classes.plan)}>Enterprise</span>
                  <span className={classnames(classes.price)}>£30 <span className={classnames(classes.month)}>/ MONTH</span></span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, authActions)(HomeView)

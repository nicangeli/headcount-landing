import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions as authActions } from '../../redux/modules/auth'
import Paper from 'material-ui/lib/paper';
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import CircularProgress from 'material-ui/lib/circular-progress';

const mapStateToProps = (state) => ({
  isLoggingIn: state.auth.get('isLoggingIn')
})
export class LoginView extends Component {

  login () {
    const email = this.refs.email.refs.input.value
    const password = this.refs.password.refs.input.value
    this.props.loginRequest(email, password)
  }

  render () {
    const style = {
      margin: 12,
    };
    const { isLoggingIn } = this.props
    if(isLoggingIn) {
      return (
        <CircularProgress />
      )
    }
    return (
      <div className='row'>
        <h1>Login to Nethanie</h1>
        <TextField
          hintText='example@nethanie.com'
          ref='email' />
        <br />
        <TextField
          type='password'
          ref='password' />
        <br />
        <RaisedButton
          onClick={this.login.bind(this)}
          label="Login"
          secondary={true}
          style={style} />
      </div>
    )
  }
}

export default connect(mapStateToProps, authActions)(LoginView)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import TextField from 'material-ui/lib/text-field'
import RaisedButton from 'material-ui/lib/raised-button'
import Toggle from 'material-ui/lib/toggle'
import { actions as jobActions } from 'redux/modules/jobs'

const mapStateToProps = (state) => ({
  questions: state.jobs.get('questions')
})
export class AddJobView extends Component {
  addQuestion () {
    this.props.addQuestion()
  }
  createJob () {
    let jobTitle = this.refs.jobTitle.getValue()
    let jobDescription = this.refs.jobDescription.getValue()
    this.props.createJob(jobTitle, jobDescription)
  }
  titleChanged (index, e) {
    this.props.updateQuestionTitle(index, e.target.value)
  }
  isRequiredChanged (index, e) {
    this.props.updateQuestionRequired(index, e.target.checked)
  }
  render () {
    const toggleStyles = {
      block: {
        maxWidth: 250,
      },
      toggle: {
        marginBottom: 16,
      },
    }
    let questions = this.props.questions.map((question, index) => {
      return (
        <div key={index}>
          <label>Question Title</label>
          <TextField
            onChange={this.titleChanged.bind(this, index)} />
          <div style={toggleStyles.block}>
            <Toggle
                 label="Is Required"
                 defaultToggled={false}
                 onToggle={this.isRequiredChanged.bind(this, index)}
                 style={toggleStyles.toggle} />
          </div>
        </div>
      )
    })
    return (
      <div>
        <h1>Add Job</h1>
        <label>Job Title</label>
        <TextField
          ref='jobTitle' />
        <label>Job Description</label>
        <TextField
          ref='jobDescription'
          multiLine={true} />
        <h2>Job Questions</h2>
        {questions}
        <RaisedButton
          onClick={this.addQuestion.bind(this)}
          label="Add New Question" />
        <RaisedButton
          onClick={this.createJob.bind(this)}
          label="Create Job"
          secondary={true} />
      </div>
    )
  }
}

export default connect(mapStateToProps, jobActions)(AddJobView)

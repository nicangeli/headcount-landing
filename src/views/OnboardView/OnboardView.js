import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/lib/text-field';
import { actions as userActions } from '../../redux/modules/user'
import { connect } from 'react-redux'
import CircularProgress from 'material-ui/lib/circular-progress';
import RaisedButton from 'material-ui/lib/raised-button'

const mapStateToProps = (state) => ({
  user: state.user.get('user'),
  isFetching: state.user.get('isFetching'),
  error: state.user.get('error')
})
export class OnboardView extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    this.props.fetchUserRequest()
  }

  companyDescription(user) {
    if (user) {
      return `Tell us more about ${user.get('companyName')}`
    }
  }

  handleClickNext () {
    let description = this.refs.description.refs.input.refs.input.value
    this.props.addCompanyDescription(description)
  }

  render () {
    const { isFetching, user, error } = this.props
    if (isFetching) {
      return <CircularProgress />
    }
    return (
      <div className='row'>
        <div className='small-12 columns'>
          <h1>Step 1</h1>
          <label>{this.companyDescription(user)}</label>
          <p>This will appear on your job site. Its important to sell yourself to potential candidates here.</p>
          <TextField ref='description' multiLine={true} />
        </div>
        <div className='small-12 columns'>
          <RaisedButton
            onClick={this.handleClickNext.bind(this)}
            label='Next'
            secondary={true} />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, {
  fetchUserRequest: userActions.fetchUserRequest,
  addCompanyDescription: userActions.addCompanyDescription
})(OnboardView)

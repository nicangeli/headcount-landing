import React, { Component } from 'react'
import { Link } from 'react-router'
import { actions as userActions } from '../../redux/modules/user'
import { actions as authActions } from '../../redux/modules/auth'
import CircularProgress from 'material-ui/lib/circular-progress'
import Menu from 'material-ui/lib/menus/menu'
import { connect } from 'react-redux'
import MenuItem from 'material-ui/lib/menus/menu-item'

const mapStateToProps = (state) => ({
  user: state.user.get('user'),
  isFetching: state.user.get('isFetching'),
  error: state.user.get('error')
})
export class DashboardView extends Component {
  componentDidMount () {
    this.props.fetchUserRequest()
  }
  handleLogout () {
    this.props.logout()
  }
  render () {
    const style = {
      marginRight: 32,
      marginBottom: 32,
      float: 'left',
      position: 'relative',
      zIndex: 0,
    };

    const { user, isFetching, error } = this.props
    if (isFetching) {
      return <CircularProgress />
    }
    if (error) {
      return (
        <p>Error: {error}</p>
      )
    }
    return (
      <div>
        <h1>{user.get('firstName')}, your personal dashboard </h1>
          <Menu style={style}>
            <MenuItem containerElement={<Link to='/jobs/add' />} primaryText="Add Job Listing" />
            <MenuItem primaryText="Add Team Member" />
            <MenuItem primaryText="Help &amp; feedback" />
            <MenuItem primaryText="Settings" />
            <MenuItem onClick={this.handleLogout.bind(this)} primaryText="Sign out" />
          </Menu>
      </div>
    )
  }
}

export default connect(mapStateToProps, {
  fetchUserRequest: userActions.fetchUserRequest,
  logout: authActions.logout
})(DashboardView)

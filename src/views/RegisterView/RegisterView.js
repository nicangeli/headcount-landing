import React, { Component } from 'react'
import { connect } from 'react-redux'
import { actions as authActions } from '../../redux/modules/auth'
import TextField from 'material-ui/lib/text-field';
import RaisedButton from 'material-ui/lib/raised-button';
import CircularProgress from 'material-ui/lib/circular-progress';

const mapStateToProps = (state) => ({
  isRegistering: state.auth.get('isRegistering'),
  email: state.auth.get('email')
})
export class RegisterView extends Component {
  register () {
    const email = this.refs.email.refs.input.value
    const firstName = this.refs.firstName.refs.input.value
    const lastName = this.refs.lastName.refs.input.value
    const companyName = this.refs.companyName.refs.input.value
    const password = this.refs.password.refs.input.value
    this.props.registerRequest(email, firstName, lastName, companyName, password)
  }

  companyLogo () {
    if (this.props.email) {
      let company = this.props.email.split('@')[1]
      return `https://logo.clearbit.com/${company}?s=128`
    }
  }

  getEmail () {
    return this.props.email
  }

  getCompany () {
    if (this.props.email) {
      return this.props.email.split('@')[1].split('.')[0]
    }
  }

  render () {
    const style = {
      margin: 12,
    };
    const { isRegistering } = this.props
    if(isRegistering) {
      return (
        <CircularProgress />
      )
    }
    return (
      <div
        className='row'>
        <div
          style={
              {
                fontFamily: `'Varela Round', sans-serif`,
                color: 'rgb(75,89,94)'
              }
            }
          className='small-12 columns'>
          <img src={this.companyLogo()} />
          <h1
            style={
              {
                fontFamily: `'Varela Round', sans-serif`,
                fontSize: '30px'
              }
            }>Register with Headcount</h1>
          <p>You're just moments away from having your first job post live</p>
            <TextField
              defaultValue={this.getEmail()}
              hintText='example@nethanie.com'
              ref='email' />
            <br />
            <TextField
                hintText='Nick'
                ref='firstName' />
            <br />
            <TextField
              hintText='Angeli'
              ref='lastName' />
            <br />
            <TextField
              defaultValue={this.getCompany()}
              type='text'
              hintText='Company Name'
              ref='companyName' />
            <br />
            <TextField
              type='password'
              ref='password' />
            <br />
            <RaisedButton
              onClick={this.register.bind(this)}
              label="Login"
              secondary={true}
              style={style} />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, authActions)(RegisterView)

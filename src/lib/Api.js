import axios from 'axios'
const BASE_URL = 'http://headcounthq.com:8080/api/'

export default new class Api {
  authorize(credentials) {
    return axios({
      method: 'POST',
      url: `${BASE_URL}login`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: credentials
    })
  }
  register (user) {
    return axios({
      method: 'POST',
      url: `${BASE_URL}register`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: user
    })
  }
  createJob (companyName, title, jobDescription, questions) {
    let token = localStorage.getItem('token')
    return axios({
      method: 'POST',
      url: `${BASE_URL}${companyName}/jobs`,
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },
      data: {
        title,
        jobDescription,
        questions
      }
    })
  }
  storeItem(token) {
    return localStorage.setItem('token', token)
  }
  clearItem(key) {
    return localStorage.removeItem(key)
  }
  fetchUser () {
    let token = localStorage.getItem('token')
    return axios({
      method: 'GET',
      url: `${BASE_URL}user`,
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },
      data: null
    })
  }
  addCompanyDescription (description) {
    let token = localStorage.getItem('token')
    return axios({
      method: 'POST',
      url: `${BASE_URL}description`,
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },
      data: {
        description
      }
    })
  }
}

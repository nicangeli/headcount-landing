import { fromJS } from 'immutable'

export const CREATE_JOB = 'CREATE_JOB'
export const CREATE_JOB_SUCCESS = 'CREATE_JOB_SUCCESS'
export const CREATE_JOB_FAILED = 'CREATE_JOB_FAILED'
export const ADD_QUESTION = 'ADD_QUESTION'
export const UPDATE_QUESTION_TITLE = 'UPDATE_QUESTION_TITLE'
export const UPDATE_QUESTION_REQUIRED = 'UPDATE_QUESTION_REQUIRED'

export const createJob = (jobTitle, jobDescription) => ({
  type: CREATE_JOB,
  jobTitle,
  jobDescription
})

export const addQuestion = (title) => ({
  type: ADD_QUESTION,
  title
})

export const updateQuestionTitle = (index, title) => ({
  type: UPDATE_QUESTION_TITLE,
  index,
  title
})

export const updateQuestionRequired = (index, isRequired) => ({
  type: UPDATE_QUESTION_REQUIRED,
  index,
  isRequired
})

export const actions = {
  createJob,
  addQuestion,
  updateQuestionTitle,
  updateQuestionRequired
}

const emptyQuestion = fromJS({
  title: '',
  isRequired: false
})

const defaultState = fromJS({
  questions: [emptyQuestion]
})

const ACTION_HANDLERS = {
  [CREATE_JOB]: (state, action) => {
    return state.merge({
      'title': action.jobTitle,
      'description': action.jobDescription
    })
  },
  [ADD_QUESTION]: (state, action) => {
    return state.update('questions', questions => questions.push(emptyQuestion))
  },
  [UPDATE_QUESTION_TITLE]: (state, action) => {
    return state.update('questions', questions => {
      let question = questions.get(action.index)
      return questions.set(action.index, question.set('title', action.title))
    })
  },
  [UPDATE_QUESTION_REQUIRED]: (state, action) => {
    return state.update('questions', questions => {
      let question = questions.get(action.index)
      return questions.set(action.index, question.set('isRequired', action.isRequired))
    })
  }
}

export default function jobsReducer (state = defaultState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

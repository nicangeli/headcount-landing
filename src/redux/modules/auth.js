import { fromJS } from 'immutable'

export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGOUT = 'LOGOUT'
export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const REGISTER_ERROR = 'REGISTER_ERROR'
export const SET_EMAIL = 'SET_EMAIL'

export const loginRequest = (email, password) => ({
  type: LOGIN_REQUEST,
  email,
  password
})

export const loginSuccess = (token, user) => ({
  type: LOGIN_SUCCESS,
  token,
  user
})

export const loginError = () => ({
  type: LOGIN_ERROR
})

export const logout = () => ({
  type: LOGOUT
})

export const setEmail = (email) => ({
  type: SET_EMAIL,
  email
})

export const registerRequest = (email, firstName, lastName, companyName, password) => ({
  type: REGISTER_REQUEST,
  email,
  firstName,
  lastName,
  companyName,
  password
})

export const registerSuccess = () => ({
  type: REGISTER_SUCCESS
})

export const registerError = () => ({
  type: REGISTER_ERROR
})

export const actions = {
  loginRequest,
  loginSuccess,
  loginError,
  logout,
  registerRequest,
  registerSuccess,
  registerError,
  setEmail
}

const defaultState = fromJS({
  isLoggingIn: false,
  token: null,
  loginError: null
})

const ACTION_HANDLERS = {
  [LOGIN_REQUEST]: (state, action) => {
    return state.set('isLoggingIn', true)
  },
  [LOGIN_SUCCESS]: (state, action) => {
    let s = state.set('isLoggingIn', false)
    return s.set('token', action.token)
  },
  [LOGIN_ERROR]: (state, action) => {
    return state.set('loginError', action.payload)
  },
  [LOGOUT]: (state, action) => {
    return state.remove('token')
  },
  [REGISTER_REQUEST]: (state, action) => {
    return state.set('isRegistering', true)
  },
  [REGISTER_SUCCESS]: (state, action) => {
    let s = state.set('registerError', false)
    return s.set('isRegistering', false);
  },
  [REGISTER_ERROR]: (state, action) => {
    let s = state.set('registerError', true)
    return s.set('isRegistering', false);
  },
  [SET_EMAIL]: (state, action) => {
    return state.set('email', action.email)
  }
}

export default function authReducer (state = defaultState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

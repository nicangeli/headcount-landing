import { fromJS } from 'immutable'

import { LOGIN_SUCCESS, LOGOUT } from './auth'

export const FETCH_USER_REQUEST = 'FETCH_USER_REQUEST'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'
export const ADD_COMPANY_DESCRIPTION = 'ADD_COMPANY_DESCRIPTION'
export const ADD_COMPANY_DESCRIPTION_SUCCESS = 'ADD_COMPANY_DESCRIPTION_SUCCESS'
export const ADD_COMPANY_DESCRIPTION_ERROR = 'ADD_COMPANY_DESCRIPTION_ERROR'

export const fetchUserRequest = () => ({
  type: FETCH_USER_REQUEST
})

export const addCompanyDescription = (description) => ({
  type: ADD_COMPANY_DESCRIPTION,
  description
})

export const actions = {
  fetchUserRequest,
  addCompanyDescription
}

const defaultState = fromJS({
  user: null,
  isFetching: true,
  isAddingDescription: false
})

const ACTION_HANDLERS = {
  [LOGIN_SUCCESS]: (state, action) => {
    return state.set('user', action.user)
  },
  [FETCH_USER_REQUEST]: (state, action) => {
    return state.set('isFetching', true)
  },
  [FETCH_USER_SUCCESS]: (state, action) => {
    return state.merge({
      user: action.user,
      error: null,
      isFetching: false
    })
  },
  [FETCH_USER_ERROR]: (state, action) => {
    return state.merge({
      user: null,
      isFetching: false,
      error: 'Error grabbing user'
    })
  },
  [ADD_COMPANY_DESCRIPTION]: (state, action) => {
    return state.merge({
      isAddingDescription: true
    })
  },
  [ADD_COMPANY_DESCRIPTION_SUCCESS]: (state, action) => {
    return state.merge({
      isAddingDescription: false
    })
  },
  [ADD_COMPANY_DESCRIPTION_ERROR]: (state, action) => {
    return state.merge({
      isAddingDescription: false
    })
  },
  [LOGOUT]: (state, action) => {
    return state.remove('user')
  }
}

export default function userReducer (state = defaultState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}

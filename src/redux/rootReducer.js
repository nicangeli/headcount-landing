import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import auth from './modules/auth'
import user from './modules/user'
import jobs from './modules/jobs'

export default combineReducers({
  auth,
  user,
  router,
  jobs
})

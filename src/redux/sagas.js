import { cancel, fork, take, call, put, select } from 'redux-saga/effects'
import { takeEvery, takeLatest } from 'redux-saga'
import { browserHistory } from 'react-router'
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
  SET_EMAIL,
  loginSuccess,
} from './modules/auth'

import {
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
  ADD_COMPANY_DESCRIPTION,
  ADD_COMPANY_DESCRIPTION_SUCCESS,
  ADD_COMPANY_DESCRIPTION_ERROR
} from './modules/user'

import {
  CREATE_JOB,
  CREATE_JOB_SUCCESS,
  CREATE_JOB_FAILED
} from './modules/jobs'

import Api from '../lib/Api'

function *authorize(email, password) {
  try {
    const res = yield call(Api.authorize, {
      email,
      password
    })
    const { token, user } = res.data
    yield put(loginSuccess(token, user))
    yield call(Api.storeItem, token)
    browserHistory.push('/dashboard')
  } catch (err) {
    console.log(err)
    yield put({type: LOGIN_ERROR, payload: err})
  }
}

function *register(email, firstName, lastName, companyName, password) {
  try {
    const res = yield call(Api.register, {
      email,
      firstName,
      lastName,
      companyName,
      password
    })
    const { token } = res.data
    yield put({type: REGISTER_SUCCESS})
    yield call(Api.storeItem, token)
    browserHistory.push('/onboard')
  } catch (err) {
    console.log(err)
    yield put({type: REGISTER_ERROR})
  }
}

function *createJob(jobTitle, jobDescription) {
  try {
    const state = yield select()
    const companyName = state.user.getIn(['user', 'companyName'])
    const questions = state.jobs.get('questions')
    const res = yield call(Api.createJob, companyName, jobTitle, jobDescription, questions)
    yield put({
      type: CREATE_JOB_SUCCESS
    })
    browserHistory.push('/dashboard')
  } catch (err) {
    console.log(err)
    yield put({type: CREATE_JOB_FAILED})
  }
}

export function *loginFlow() {
  while (true) {
    let { email, password } = yield take(LOGIN_REQUEST)
    const task = yield fork(authorize, email, password)
    yield take(LOGOUT)
    yield cancel(task)
    yield call(Api.clearItem, 'token')
    browserHistory.push('/')
  }
}

export function *logoutFlow() {
  while (true) {
    yield take(LOGOUT)
    yield call(Api.clearItem, 'token')
    browserHistory.push('/')
  }
}

export function *registerFlow() {
  while (true) {
    let { email, firstName, lastName, companyName, password } = yield take(REGISTER_REQUEST)
    const task = yield fork(register, email, firstName, lastName, companyName, password)
  }
}

export function *createJobFlow() {
  while (true) {
    let { jobTitle, jobDescription } = yield take(CREATE_JOB)
    const task = yield fork(createJob, jobTitle, jobDescription)
  }
}

function *fetchUser(action) {
  try {
    const res = yield call(Api.fetchUser)
    yield put({
      type: FETCH_USER_SUCCESS,
      user: res.data
    })
  } catch (err) {
    console.log(err)
    yield put({type: FETCH_USER_ERROR})
  }
}

export function *fetchUserIfNeeded() {
  yield* takeEvery(FETCH_USER_REQUEST, fetchUser)
}

export function *handleSetEmail() {
  while (true) {
    let { email } = yield take(SET_EMAIL)
    browserHistory.push('/register')
  }
}

function *addCompanyDescription (action) {
  console.log(action)
  try {
    const res = yield call(Api.addCompanyDescription, action.description)
    yield put({
      type: ADD_COMPANY_DESCRIPTION_SUCCESS
    })
    browserHistory.push('/jobs/add')
  } catch (err) {
    console.log(err)
    yield put({type: ADD_COMPANY_DESCRIPTION_ERROR})
  }
}

export function *handleAddCompanyDescription () {
  yield* takeEvery(ADD_COMPANY_DESCRIPTION, addCompanyDescription)
}

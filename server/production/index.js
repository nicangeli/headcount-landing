var express = require('express')

var app = express()
app.engine('html', require('ejs').renderFile);
app.use(express.static(__dirname + '/../../dist/'));
app.set('views', __dirname + '/../../dist/');
app.set('view engine', 'ejs');
app.set('port', process.env.PORT || 8081);

app.get('*', function(req,res) {
  res.render('index.html')
})

app.listen(app.get('port'))
